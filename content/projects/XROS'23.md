+++
date = "2023-04-03T00:00:00-05:00"
title = "AR based personal assistant integrated with Artifical Intelligence @XROS'23"
description = "XR Extended Reality Open Source Project"
slug = "xros'23"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["xros", "open-source"]

[extra]
    page_identifier = "xros-23-harshil-jani"
+++

Ongoing