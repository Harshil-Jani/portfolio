+++
date = "2022-05-04T00:00:00-05:00"
title = "Performance Monitoring and Data Visualization for Geant-4 @GSOC'22"
description = "Google Summer of Code 2022 project at CERN-HSF"
slug = "gsoc'22"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["gsoc", "open-source"]

[extra]
    page_identifier = "gsoc-22-harshil-jani"
+++

[Project Repo](https://github.com/amadio/g4run)

Geant4 is a scientific software used for partical simulations. The project was about Data Visualization and Performance Monitoring between any two commits or importantly over the tags.

We used `perf` which is tool by Linux Kernel to profile the software while compiling it and using `d3.js` I have created some visualizations based on required metrics which were really importance for performance.

End goal was to provide this inside the Jenkins Pipeline for the CERN and Geant4 Developement Team so that they can closely take a look at it and get to know that on each commit which functions were affected with poor performance or had been improved. This helps them in prioritizing the work and makes the developement workflow more robust.

I have written a Book on the entire project and my blog was also posted on CERN-HSF's Blog Forum.


[Book : GSoC'22 @CERN-HSF, ft. Harshil Jani](https://harshil-jani.github.io/GSOC-book/)

[Blog Post : Geant4 GSoC'22](https://hepsoftwarefoundation.org/gsoc/blogs/2022/blog_Geant4_HarshilJani.html)

[Article on Perf](https://medium.com/@harshiljani2002/linux-perf-profiling-227d17101bd6)

[Article on Data Visualization](https://medium.com/@harshiljani2002/visualizing-data-on-web-390d42e52a51)
