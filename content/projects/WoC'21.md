+++
date = "2021-12-15T00:00:00-05:00"
title = "LLVM Compiler : Sementic check for Flang @WoC-2.0"
description = "Winter of Code 2022"
slug = "woc'21"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["woc", "open-source"]

[extra]
    page_identifier = "woc'21"
+++

Worked for the Flang90 Language compiler in LLVM over the improvement patch for OpenMP API with the `target enter data` and `target exit data`.

Commit Link : [Harshil's Merged Patch in LLVM](https://github.com/llvm/llvm-project/commit/bea53eead1de84a28affc6a7cbf88f87a258fed4)

### "The device expression must evaluate to non-negative integer."

My tasks were to find out where this target enter data and target exit data failed.

I was expected to know what device expressions were and after knowing them in detail from the OpenMP Specification sheet and the Example sheet, I had to craft a test case where on passing the negative number inside the device clause of both the constructs it should compile with no errors returned. Now, This was the thing. There has to be some error on passing the negative number and that was what I took up as work.

There is a target construct that deals with the device data environments for handling variables created in data environment. This construct has some directives, `target data` which is structured directive or `target enter data` and `target exit data` which are unstructured directives and also standalone. Now use of unstructured directives is creation and deletion of data on the device at any point within host code. So, in the specifications sheet as mentioned we have device clause for unstructured data constructs. As arguments we can provide the device id as a non negative integer.

I somehow, With the help of OpenMP example sheet managed to write a test file. At very first I was unable to compile the file as I have not exported the flang compiler to my system path. So, with the help of some guidance I did that and then got file compiled successfully. Now, I dumped the whole parse tree of the FORTRAN code. Inside the llvm flang files there is one file named as `parse-tree.h` header file and we can look into this header file for understanding the different nodes of the parse tree. Now `parse-tree.h` will allow you to make a plan, like you can get `OpenMPStandaloneConstruct` within in you need to extract `OmpClause`, which is a `std::variant` so you may need to extract Device, then look at definition of `Device` to extract `Scalar`, and then `Integer`, and then the `Expr` and then check the sign

We had some brainstorming around `std::variant` and `OpenMPStandaloneConstruct` and figured out which function was working in our code for execution of the test file code that I wrote. Inserted some print statements within some functions and with some trial and error, found out the function responsible for working. It was the following function :

```Cpp
void OmpStructureChecker::Enter(const parser::OpenMPConstruct &x)
```

Now, Inside this function there was a need to write another function dealing with the sign of the Device expression. So, I created the function named

```Cpp
void CheckDeviceExpr(const parser::OpenMPConstruct &x)
```

Initially, I thought the function required taking every node element of the parse tree. But on further research it turned out that from just `OpenMPStandaloneConstruct` we can have access to `OmpClause` and from there extract the definition of `Device` clause. I somehow, On the search from various files I found something known as ```llvm::omp::clause::OMPC_device``` This brought me closer to my implementation.

For next few days I was trying was to extract the `OMPC_device` clause somehow from the parser path into my function. But, I was unable to get any idea. I again tried a hit of trial and error and got myself tangled into 3 scenarios.

1. 
```cpp
if (llvm::omp::Clause::OMPC_device)
``` 
This won’t work as this is clause and won’t return any boolean values, So it failed to compile.

2. 
```cpp
if (FindClause(llvm::omp::Clause::OMPC_device))
``` 
Now here the main thing is that we are not using 
```cpp
void function_name::some_constructor(const our_arg)
``` 
but instead we use 
```cpp
void function_name (const our_arg)
``` 

So, FindClause won’t be defined in it. It is pre-defined for some used constructors such as `::Enter ::ChecksOnOrderedAsStandalone` etc. So, this also failed.

3. 
```cpp
std::cout<<llvm::omp::Clause::OMPC_device
```
this however was worst thing to do. It is clause so it won’t work this way. It needs some iterative prints.

Then I had again omitted the whole function and started staring codes again. Suddenly, I found some functions with valid comments. The first set of functions says **// Use when clause falls under ‘struct OmpClause’** in `parse-tree.h`. 

When we compare this with the parse Tree of Test File then we have Device Expression under Omp Clause. 
```cpp
#define CHECK_REQ_SCALAR_INT_CLAUSE(X, Y)
``` 
This Macro is what the Spec sheet says. Our argument is scalAr integer which will have positive values checked already as seen in it’s function definition. What I did was simply changed

`CHECK_SIMPLE_CLAUSE(Device, OMPC_device)` with `CHECK_REQ_SCALER_INT(Device,OMPC_device)` and the test file throwed me error as expected. So, Now I felt I made it. I thought this is it. My first thing in LLVM got completed. Here was the new twist tho. It just accepted the positive numbers. For me it was to allow non-negatives. So I had problems with passing 0 into this. Then, What I did is created a new function inside directive calls where passing 0 was allowed as well. And finally made it.

Then I created a patch for this and with the guidance from my mentor, got it uploaded on Phabricator where It was reviewed for couple of days with some minor changes which taught me more clean code writing. And then with the final revision it was accepted.