+++
date = "2023-04-14T00:00:00-05:00"
title = "Matrix Bridge binary for Qaul Application @GSoC'23"
description = "Google Summer of Code 2023 project at Freifunk"
slug = "gsoc'23"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["gsoc", "open-source"]

[extra]
    page_identifier = "gsoc-23-harshil-jani"
+++

Ongoing