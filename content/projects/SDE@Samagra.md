+++
date = "2022-09-12T00:00:00-05:00"
title = "SDE Intern Work Report @Samagra"
description = "Describing my work done at Samagra"
slug = "sde-samagra"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["sde", "intern", "open-source"]

[extra]
    page_identifier = "sde-samagra"
+++

# :rocket: Samarth Admin
#### Project Description
The State Government of Himachal Pradesh has embarked on an ambitious state-wide systemic transformation programme to improve the quality of education in the state’s 14000 elementary government schools.

Our Admin helps them in achieving the management of the Data keeping in mind three Admin users 
- State Admin
- District Admin
- Block Admin

and Allowing them to perform CRUD based on their access to the specific data. I got a complete end to end use case for the work. 
Data was coming from three paths (Dataprovider) **Hasura**, **ESamwaad Custom data provider** and **JSON Server** for user roles. 
We also implemented the functionality of role based operations on admin.
#### Technology 
React, Typescript, Hasura Data Management, PostgreSQL, React-Admin, GraphQL
#### Proof of Work
[Contributions](https://github.com/Samarth-HP/admin-ts/graphs/contributors)

# :rocket: `ra-data-samagra` npm module 
#### Project Description
A Data Provider for react admin tailored to target the Samagra APIs consisting of GraphQL and Fusion Auth endpoints. It helps in mixing the data coming from all three end points of ESamwaad and ShikshaSaathi from Fusion Auth and also from Hasrua. This combination can be levaraged on any frontend using react admin.
#### Technology
npm registry, react, typescript, react-admin, hasura, graphQL
#### Proof of Work
[Github](https://github.com/Harshil-Jani/ra-data-samagra)

[npm package](https://www.npmjs.com/package/ra-data-samagra?activeTab=readme)

# :rocket: Data Mapping Console
#### Project Description
The Data mapping console will allow creating a mapping for any template created using the doc generator service. It was extensively used by the Product Managers for mapping Templates with ODK Form submissions.
- Provide a user friendly experience, 
- Eliminate errors in data mapping,
- Reduce developer effort
- Reduce turnaround time

#### Technology
NextJS, OAuth2, Typescript
#### Proof of Work
- [Private Repo](https://github.com/Samagra-Development/Data-Mapping-Console) This repo is private so externally you cannot access.
- [Website](https://docgen.luezoid.com/) This website has internal APIs which we don't want to expose outside of organization.

# :rocket: ULP-Devops
#### Project Description
Had helped in maintaining the pipeline and ansible related issues for ULP services.
#### Technology
Jenkins, Docker, Ansible
#### Proof of Work
Most of the work is resolved in Jenkins UI itself. 
But I wrote a guide on Samagra DevOps which could be find here.
- [Samagra Devops guide](https://github.com/Samagra-Development/Samagra-DevOps-Guide)

# :rocket: Tauri Build for UCI
#### Project Description
Was interested in RustLang and had got a task to create the Rust based desktop application using Tauri. It did magic for the builds but need to wait for the beta to get stable so we can have android and iOS builds as well.

#### Technology
Rust, Tauri, Typescript, NextJS

#### Proof of Work
- Discussion Thread for onboarding tauri stack at Samagra. [Issue Link](https://github.com/samagra-comms/uci-web-channel/issues/60)
- Raised Pull Request for the PoC what could be potential changes. [PR Link](https://github.com/samagra-comms/uci-web-channel/pull/88)