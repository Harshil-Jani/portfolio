+++
date = "2023-04-15T00:00:00-05:00"
title = "Key or Pre-Hash image based planning module @Rust-Bitcoin"
description = "Summer of Bitcoin Project @Rust-Bitcoin"
slug = "sob'23"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["sob", "open-source"]

[extra]
    page_identifier = "sob-23-harshil-jani"
+++

When creating a transaction with multiple parties, it is important to agree on a transaction feerate to prevent denial of service attacks. To this end, when trying to estimate the worst-case satisfaction of weights, rust-miniscript does the absolute worst-case analysis. While this is a okay solution, we often end up overpaying for spend paths that might not be executed. 

The idea here would be to improve the worst case analysis by considering key availability and offer better and more economical results while signing the transactions.

[Project Proposal](https://docs.google.com/document/d/1c0E_H2gip7mH2Q3x3qwTE6RQnuCa2qgCLkcHhQ1IZwY/edit#heading=h.ridf1ye86jvt)

#### Related PRs : 
- [Add method to convert RawPkh](https://github.com/rust-bitcoin/rust-miniscript/pull/557)