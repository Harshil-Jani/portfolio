+++
title = "The Harshil Jani"
description = "Personal Blog and Project portfolio of Mr. Harshil Jani"
sort_by = "date"
template = "index.html"
page_template = "page.html"
+++

{{ tab_nav_box(
        id = "home"
        class = "mb-5"
        tab_titles = [
            "👋 About me",
            "📝 My Hobbies",
            "✉ Contact Me"
        ]
        tab_contents = [
            "I am final year Undergraduate Engineering Student and would graduate by 2024. My majors are in Electronics and Communications and minors in Computer Science. Rust is my favorite programming language. I am not an enterpreneur but yet I sail my own boat.",
            "I play chess a lot. Chocolatey Thickshake is my favorite cold-drink and when I am in Surat for my academics I prefer it twice a week. At my hometown, I prefer cycling with my friends. I have been a participant continuously for 6 Years in 14 Kms Full Cycling Marathon. ",
            "I am active on any and many social profiles. Approaching me is so easy so don't feel hesitated to reach out. Keep the questions to the point for better delivery of discussion. You may find some links to my socials above."
        ]
    )
}}

## Sponser Me

<html>
<head>
  <style>
    .github {
      background-color: white;
      color: white;
      display: flex;
      color: black;
      align-items: ;
    }
    .sponsers {
      width: 25px;
      height: 25px;
    }
  </style>
</head>
<body>
  <a class="github" href="https://github.com/sponsors/Harshil-Jani/" target="_blank">
    <img class="sponsers"src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="GitHub Logo">
    <p>Harshil-Jani</p>
  </a>
</body>
</html>
