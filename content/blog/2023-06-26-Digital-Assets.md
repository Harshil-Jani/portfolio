+++
date = "2023-06-26T22:24:00-05:00"
title = "Hitting Virtual Reality Physically"
description = "A good reading experience"
slug = "digital-asset"
draft = false

[taxonomies]
    categories = ["philosophy"]
    tags = ["digital","physical-world","virtual-world"]

[extra]
    image = { path = "images/virtual.jpg", alt = "Drawbacks of Virtual Reality", visible_in_page = true, visible_in_section = true }
    page_identifier = "bunk"
+++

Few days back, I was reading an article for a seminar talk in summer of bitcoin student program and I found an interesting line.

<b><q>if you have an apple and I have an apple, and we swap apples — we each end up with only one apple. But if you and I have an idea and we swap ideas — we each end up with two ideas.</q></b>

\- Charles F. Brannan (1949)

This explain the core difference between the physical world and the virtual world. Let's go back to our 4th class grammar lessons. There was a concept of abstract nouns and non-abstract nouns.

- **Abstract Noun :** a noun denoting an idea, quality, or state rather than a concrete object, e.g. truth, danger, happiness.

- **Non-Abstract Noun :** A concrete noun identifies something material and non-abstract, such as a chair, a house, or an automobile.

Again, There comes a concept of physical world and virtual world. Physical world is something where you interact directly with the object. In virtual world, The object is copied multiple times and you get indirectly the copy of the thing.

If you exchange abstract things in physical world or virtual world it will be at both ends. It may be considered as a reference copy. You share it and refer to same thing and not actually have a copy of it. To make my words simpler, Let's take an example of a long distance relationship where you both share feelings (an abstract thing) and you even share it in physical world too.

When it comes to non-abstract things then you actually pass it on in physical world. But you share strictly only the copies of it. Again analogus example could be that you want to go for a house search and look for the house. With emerging technology the broker will try to convince you to look at multiple houses via Virtual Reality and Augmented Reality solutions. But would you choose to hit on it ? Because, You would not be satisfied. House is something where you don't just want to see it internally or even externally. Real reality is more important in such critical cases. Afterall house is generally the most expensive asset which anyone would ever buy. So you would want to go over there and physically look at the weather, water facility, locality, area growth projection bla bla. So, Sharing an abstract thing in virtual world is not good.

To make it sound like a Stanford B-School knowledge, I am creating a matrix below

<style>
  table {
    border-collapse: collapse;
    width: 100%;
    border: 2px solid black;
  }
  th, td {
    padding: 8px;
    text-align: left;
    border: 1px solid black;
  }
</style>

<!-- I own the Gunnubear matrix and if anyone will try to copy this, I will hit your face with my this comment and text which I am writing on 26th June 2023 and this will also get registered inside my gitlab commit records. -->

**Gunnubear Matrix**

<table>
<th></th>
<th>Physical</th>
<th>Virtual</th>
  <tr>
  <th>Abstract</th>
    <td>Shared Mutually</td>
    <td>Shared Mutually</td>
    
  </tr>
  
  <tr>
    <th>Non-Abstract</th>
    <td>Shared Exclusively</td>
    <td>Shared copies indirectly</td>
  </tr>
</table>

<br/>
With this I urge startups, college grads and lots of potential people who want to work towards something impactful to leave such blandishments and look for the real problem. Especially at the place where you don't have awareness and education enough for people to yet operate basic tech among millennials there is no reason to waste important resources at wrong thing. Again, I am just trying to make my point clear which could be wrong but if this impact atleast 1% of the actual readers, I may prevail to do my job.
