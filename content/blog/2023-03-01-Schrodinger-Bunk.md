+++
date = "2023-03-01T22:24:00-05:00"
title = "Schrodinger Bunk"
description = "Teaching my classmates how to bunk"
slug = "bunk"
draft = false

[taxonomies]
    categories = ["university"]
    tags = ["bunk"]

[extra]
    image = { path = "images/sbunk.svg", alt = "GSoC FAQs not to be asked", visible_in_page = false, visible_in_section = true }
    page_identifier = "bunk"
+++


I am an undergrad student. So, I study in university where students wants to do things poles apart from what they've been served here. As a part of daily or weekly riots amongst professors and the students, There are always spreading rumours of class bunks. 

The good part of bunks is that it divides the mesophiles into two catagories. The first one are the ones who wants the bunk. Second ones are the ones who have their attitude of - "If people go I go". 

On personal take, I am part of first catagorie. But, I have been observing this for so long now and wanted to draw out a whole new perspective to mass bunks. 

Let's start with the philosophy. There is a declared mass bunk. Separate yourself out as an individuals. Now, It remains as mass bunk untill and unless you don't ask anyone about who went and who haven't. If you find anyone who went to class, then at that instant the mass bunk fails. Untill you don't ask and don't find out the imposters, It is bunk. This is same as schrodinger's cat theory where the cat is alive untill you open the box. 

So, My urge to my fellow classmates cum batchmates is that in order to succeed at a mass bunk, please don't ask or tell who went. Individually assume the bunk and then the bunk will happen. 
